# REMP Paywall

This module integrates a Drupal site with REMP CRM. This module
allows sites to only view to REMP mebers.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/remp).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/remp).

Brainsum - [Brainsum](https://www.brainsum.hu/)

## Table of contents

* Requirements
* Installation
* Configuration
* Using custom content display
* Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

After enabling the module, you need to add the "ramp_member_only" field 
to the content type. This field will handle access for viewing.


## Configuration

    1. Navigate to Administration > Extend and enable the REMP module.
    2. Navigate to Administration > Structure > Content types.
    3. Add remp_member_only field to bundle where needs to handle access
    for view.
    4. Configure REMP crm host and sales funnel at /admin/config/remp/settings.
        Optionally, If your content display is custom, you can check the
        "Use own anonym member sections" checkbox.
    5. Go to node bundle display view admin page and enable anonym
    display mode.
    6. Change display mode to Anonym and set as you need. Anonym users
    will see this display mode.

Add new content and check in the remp_member_only field.


## Using custom content display

If your site does not use the entity view builder to render content, you must
wrap your content that is available to all users and to members only with a HTML
tag having the "remp-anonym" and "remp-member" ids respectively.


## Maintainers

The 8.x branch was created by:

 * Jozsef Dudas - [dj1999](https://www.drupal.org/u/dj1999)
 * Levente Besenyei - [lbesenyei](https://www.drupal.org/u/lbesenyei)

This module was created and sponsored by Brainsum, a Drupal development company
in Budapest, Hungary.
