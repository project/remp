<?php

namespace Drupal\remp\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\remp\RempService;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that allows privileged users to execute arbitrary PHP code.
 */
class RempLoginForm extends FormBase {

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\Client|ClientInterface
   */
  protected $httpClient;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Remp client.
   *
   * @var \Drupal\remp\RempService
   */
  protected $rempService;

  /**
   * Constructs a new UserLoginForm.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP Client.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\remp\RempService $remp_service
   *   The REMP client.
   */
  public function __construct(ClientInterface $http_client, AccountInterface $current_user, LanguageManagerInterface $language_manager, RempService $remp_service) {
    $this->httpClient = $http_client;
    $this->currentUser = $current_user;
    $this->languageManager = $language_manager;
    $this->rempService = $remp_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('remp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'remp_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('remp.config');

    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['#prefix'] = '<div id="remp-login-form-wrapper">';
    $form['#suffix'] = '</div>';

    $form['header'] = [
      '#prefix' => '<span>',
      '#suffix' => '</span>',
      '#markup' => $config->get('form_title'),
    ];

    $form['container'] = [
      '#type' => 'container',
    ];

    $form['container']['left'] = [
      '#type' => 'container',
    ];

    $form['container']['right'] = [
      '#type' => 'container',
    ];

    $form['container']['right']['title'] = [
      '#prefix' => '<span>',
      '#suffix' => '</span>',
      '#markup' => $config->get('form_member_text'),
    ];

    $form['container']['right']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#required' => TRUE,
    ];

    $form['container']['right']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
    ];

    $form['container']['right']['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'button',
        '#value' => $this->t('Log in'),
        '#ajax' => [
          'callback' => '::ajaxLogin',
          'wrapper' => 'remp-login-form-wrapper',
        ],
      ],
      'reset' => [
        '#type' => 'link',
        '#title' => $this->t('Forgot password?'),
        '#url' => Url::fromUri($config->get('host') . '/users/users/reset-password'),
      ],
    ];

    $args = $form_state->getBuildInfo()['args'];
    $referer = $args[2] ? '&referer=' . $args[2] : '';

    $free_url = FALSE;
    $paid_url = FALSE;

    // Free subscription.
    if ($free_funnel = $config->get('funnel')) {
      $free_url = $config->get('host') . '/sales-funnel/sales-funnel-frontend/show?funnel=' . $free_funnel;
      $free_url .= $referer;
    }

    if ($paid_funnel = $config->get('paid_funnel')) {
      $paid_url = $config->get('host') . '/sales-funnel/sales-funnel-frontend/show?funnel=' . $paid_funnel;
      $paid_url .= $referer;
    }

    if ($free_url || $paid_url) {
      $form['container']['left']['label'] = [
        '#prefix' => '<h4>',
        '#suffix' => '</h4>',
        '#markup' => $config->get('form_subscribe_text'),
        '#weight' => 5,
      ];

      if ($free_url) {
        $form['container']['left']['free'] = [
          '#weight' => 10,
          'cta' => [
            '#type' => 'link',
            '#title' => $config->get('form_cta_trial_label'),
            '#url' => Url::fromUri($free_url),
          ],
          'description' => [
            '#prefix' => '<div>',
            '#suffix' => '</div>',
            '#markup' => $config->get('form_cta_trial_description'),
          ],
        ];
      }

      if ($free_url && $paid_url && $config->get('form_cta_subscription_label')) {
        $form['container']['left']['or'] = [
          '#prefix' => '<div> ',
          '#suffix' => ' </div>',
          '#markup' => $this->t('OR'),
          '#weight' => 15,
        ];
      }

      if ($paid_url && $config->get('form_cta_subscription_label')) {
        $form['container']['left']['paid'] = [
          '#weight' => 20,
          'cta' => [
            '#type' => 'link',
            '#title' => $config->get('form_cta_subscription_label'),
            '#url' => Url::fromUri($paid_url),
          ],
          'description' => [
            '#prefix' => '<div>',
            '#suffix' => '</div>',
            '#markup' => $config->get('form_cta_subscription_description'),
          ],
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');
    $password = $form_state->getValue('password');

    if (!empty($email) && !empty($password)) {
      $result = $this->rempService->login($email, $password);

      if ($result['status'] == 'error') {
        $form_state->setErrorByName('email', $result['message']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Ajax submit for REMP CRM User login.
   */
  public function ajaxLogin(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#remp-login-form-wrapper', $form));
    }
    else {
      $args = $form_state->getBuildInfo()['args'];
      $currentURL = Url::fromRoute($args[0], $args[1]);
      $response->addCommand(new RedirectCommand($currentURL->toString()));
    }

    return $response;
  }

}
