<?php

namespace Drupal\remp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * REMP Configuration form.
 */
class RempConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'remp.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'remp_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('remp.config');

    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remp Host'),
      '#description' => $this->t('Url to the REMP instance.'),
      '#default_value' => $config->get('host'),
    ];

    $form['funnel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REMP Sales funnel'),
      '#description' => $this->t('The name of the sales funnel to use for new subscription limited by number of articles viewed.'),
      '#default_value' => $config->get('funnel'),
    ];

    $form['paid_funnel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REMP Sales funnel for Paid subscription'),
      '#description' => $this->t('The name of the sales funnel to use for new paid subscription.'),
      '#default_value' => $config->get('paid_funnel'),
    ];

    $form['custom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use own anonym member sections'),
      '#description' => $this->t('Check in if prepare custom anonym and member sections. In this case you need prepare two div section. For anonym id="remp-anonym". For member id="remp-member".'),
      '#default_value' => $config->get('custom'),
    ];

    $form['beam'] = [
      '#type' => 'details',
      '#title' => $this->t('Beam'),
    ];

    $form['beam']['beam_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remp Beam Host'),
      '#description' => $this->t('Url to the REMP Beam instance.'),
      '#default_value' => $config->get('beam_host'),
    ];

    $form['beam']['beam_tracker'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remp Beam Tracker url'),
      '#description' => $this->t('Url to the Beam Tracker.'),
      '#default_value' => $config->get('beam_tracker'),
    ];

    $form['beam']['beam_property'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remp Beam Property Token'),
      '#description' => $this->t('Beam property token.'),
      '#default_value' => $config->get('beam_property'),
    ];

    $form['beam']['beam_sso_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remp SSO Token'),
      '#default_value' => $config->get('beam_sso_token'),
    ];

    $form['campaign'] = [
      '#type' => 'details',
      '#title' => $this->t('Campaign'),
    ];

    $form['campaign']['campaign_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remp Campaign Host'),
      '#description' => $this->t('Url to the REMP Campaign instance.'),
      '#default_value' => $config->get('campaign_host'),
    ];

    $form['form_texts'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Form texts'),
    ];

    $form['form_texts']['form_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form title'),
      '#default_value' => $config->get('form_title'),
    ];

    $form['form_texts']['form_member_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form member text'),
      '#default_value' => $config->get('form_member_text'),
    ];

    $form['form_texts']['form_subscribe_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form subscribe text'),
      '#default_value' => $config->get('form_subscribe_text'),
    ];

    $form['form_texts']['form_cta_trial_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form CTA Trial label'),
      '#default_value' => $config->get('form_cta_trial_label'),
    ];

    $form['form_texts']['form_cta_trial_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form CTA Trial description'),
      '#default_value' => $config->get('form_cta_trial_description'),
    ];

    $form['form_texts']['form_cta_subscription_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form CTA Subscription label'),
      '#default_value' => $config->get('form_cta_subscription_label'),
    ];

    $form['form_texts']['form_cta_subscription_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form CTA Subscription description'),
      '#default_value' => $config->get('form_cta_subscription_description'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('remp.config')
      ->set('host', rtrim($form_state->getValue('host'), '/'))
      ->set('funnel', $form_state->getValue('funnel'))
      ->set('paid_funnel', $form_state->getValue('paid_funnel'))
      ->set('custom', $form_state->getValue('custom'))
      ->set('beam_host', $form_state->getValue('beam_host'))
      ->set('beam_tracker', $form_state->getValue('beam_tracker'))
      ->set('beam_property', $form_state->getValue('beam_property'))
      ->set('beam_sso_token', $form_state->getValue('beam_sso_token'))
      ->set('campaign_host', $form_state->getValue('campaign_host'))
      ->set('form_title', $form_state->getValue('form_title'))
      ->set('form_member_text', $form_state->getValue('form_member_text'))
      ->set('form_subscribe_text', $form_state->getValue('form_subscribe_text'))
      ->set('form_cta_trial_label', $form_state->getValue('form_cta_trial_label'))
      ->set('form_cta_trial_description', $form_state->getValue('form_cta_trial_description'))
      ->set('form_cta_subscription_label', $form_state->getValue('form_cta_subscription_label'))
      ->set('form_cta_subscription_description', $form_state->getValue('form_cta_subscription_description'))
      ->save();
  }

}
