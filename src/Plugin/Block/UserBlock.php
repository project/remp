<?php

namespace Drupal\remp\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Remp User' Block.
 *
 * @Block(
 *   id = "remp_user_block",
 *   admin_label = @Translation("Remp User Block"),
 * )
 */
class UserBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<div id="remp-user-block"></div>',
    ];
  }

}
