<?php

namespace Drupal\remp\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Remp User' Block.
 *
 * @Block(
 *   id = "remp_footer_block",
 *   admin_label = @Translation("Remp Footer Block"),
 * )
 */
class FooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<div id="remp-footer-container"></div>',
    ];
  }

}
