/**
 * @file
 * Attaches the behaviors for the REMP Paywall module.
 */

(($, Drupal, drupalSettings, once) => {
  Drupal.behaviors.Remp = {

    tokenData: {
      n_email: "",
      n_token: ""
    },

    settings: drupalSettings.remp,

    // Attach behavior.
    attach(context) {
      const REMP = this;

      if (drupalSettings.remp.host) {
        once('remp', 'body', context).forEach(() => {
          if (this.memberContent.length) {
            const rempId = this.memberContent.attr("data-remp-id").split(":");
            const contentEndpoint = `${drupalSettings.path.baseUrl}${drupalSettings.path.pathPrefix}remp_content/${rempId[0]}/${rempId[1]}`;
            Drupal.ajax({ url: contentEndpoint }).execute();
          }

          if (this.rempUserBlock.length) {
            const blockEndpoint = `${drupalSettings.path.baseUrl}${drupalSettings.path.pathPrefix}remp_components`;
            Drupal.ajax({ url: blockEndpoint }).execute();
          }
        });
      }
    },

    // Init remp elements.
    init() {
      this.memberContent = $("#remp-member");
      this.anonymContent = $("#remp-anonym");
      this.rempUserBlock = $("#block-rempuserblock");
    }
  };

  // Initialize Remp and hide content.
  Drupal.behaviors.Remp.init();
})(jQuery, Drupal, drupalSettings, once);
