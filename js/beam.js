/**
 * @file
 * BEAM Integration.
 */

((Drupal, $, drupalSettings, win, doc) => {
  function mock(fn) {
    return function () {
      this._.push([fn, arguments])
    }
  }
  function load(url) {
    var script = doc.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src = url;
    doc.getElementsByTagName("head")[0].appendChild(script);
  }
  win.remplib = win.remplib || {};
  var mockFuncs = {
    "campaign": "init",
    "tracker": "init trackEvent trackPageview trackCommerce",
    "iota": "init"
  };

  Object.keys(mockFuncs).forEach(function (key) {
    if (!win.remplib[key]) {
      var fn, i, funcs = mockFuncs[key].split(" ");
      win.remplib[key] = {_: []};

      for (i = 0; i < funcs.length; i++) {
        fn = funcs[i];
        win.remplib[key][fn] = mock(fn);
      }
    }
  });

  // Change URL to location of BEAM remplib.js.
  load(drupalSettings.remp.beam_host + "/assets/lib/js/remplib.js");

  Drupal.behaviors.remp_beam = {
    attach: function (context, settings) {
      if (drupalSettings.remp.beam && !drupalSettings.remp.beam.wait) {
        // Track normal page view.
        $('body', context).once('beam').each(function () {
          win.remplib.tracker.init(drupalSettings.remp.beam);
        });
      }

      $(document).once().ajaxComplete(function (event, xhr, settings) {
        if (settings.url.indexOf('/remp_content') === 0) {
          if (!drupalSettings.remp.beam.wait) {
            // Track page view for locked content based on response.
            $('body', context).once('beam').each(function () {
              win.remplib.tracker.init(drupalSettings.remp.beam);
            });
          }
        }
      });
    }
  };
})(Drupal, jQuery, drupalSettings, window, document);
