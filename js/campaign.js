/**
 * @file
 * Campaign Integration.
 */

((Drupal, $, drupalSettings, win, doc) => {
  function mock(fn) {
      return function () {
          this._.push([fn, arguments])
      }
  }
  function load(url) {
      var script = doc.createElement("script");
      script.type = "text/javascript";
      script.async = true;
      script.src = url;
      doc.getElementsByTagName("head")[0].appendChild(script);
  }
  win.remplib = win.remplib || {};
  var mockFuncs = {
      "campaign": "init",
      "tracker": "init trackEvent trackPageview trackCommerce",
      "iota": "init"
  };

  Object.keys(mockFuncs).forEach(function (key) {
      if (!win.remplib[key]) {
          var fn, i, funcs = mockFuncs[key].split(" ");
          win.remplib[key] = {_: []};

          for (i = 0; i < funcs.length; i++) {
              fn = funcs[i];
              win.remplib[key][fn] = mock(fn);
          }
      }
  });
  // Change URL to location of CAMPAIGN remplib.js.
  load(drupalSettings.remp.campaign_host + "/assets/lib/js/remplib.js");

  Drupal.behaviors.remp_campaign = {
    attach: function (context, settings) {

      $('body', context).once('campaign').each(function () {
        const rempConfig = {
          token: drupalSettings.remp.beam.token,
          campaign: {
            // required, URL host of REMP Campaign.
            url: drupalSettings.remp.campaign_host,
          },
        };

        win.remplib.campaign.init(rempConfig);
      });
    }
  };
})(Drupal, jQuery, drupalSettings, window, document);
